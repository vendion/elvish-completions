use github.com/zzamboni/elvish-completions/comp

var vm-completions = [
        &version
        &init
        &set
        &get
        &switch= (comp:subcommands [
                &list
                &info
                &create= (comp:sequence [] ^
                        &opts= [
                                [ &short=t &desc='type' ]
                                [ &short=i &desc='interface' ]
                                [ &short=n &desc='vlan-id' ]
                                [ &short=m &desc='mtu' ]
                                [ &short=a &desc='address/prefix-len' ]
                                [ &short=b &desc='bridge' ]
                                [ &short=p ]
                        ]
              )
              &vlan
              &nat
              &private
              &add
              &remove
              &destroy
        ])
        &datastore=(comp:subcommands [
                &list
                &add
                &remove
                &add
        ])
        &list
        &info
        &create=(comp:sequence [] ^
                &opts=[
                        [ &short=d &desc='datastore' ]
                        [ &short=t &desc='template' ]
                        [ &short=s &desc='size' ]
                ])
        &install
        &start
        &stop
        &console
        &configure
        &rename
        &add=(comp:sequence [] &opts=[
                [ &short=d &desc='device' ]
                [ &short=t &desc='type' ]
                [ &short=s &desc='size|switch' ]
        ])
        &startall
        &stopall
        &reset
        &poweroff
        &destroy
        &passthru
        &clone
        &snapshot
        &rollback
        &iso
        &image=(comp:subcommands [
                &list
                &create=(comp:sequence [] &opts=[
                        [ &short=d &desc='description' ]
                        [ &short=u ]
                ])
                &destroy
                &provision=(comp:sequence [] &opts=[
                        [ &short=d &desc='datastore' ]
                ])
        ])
]

set edit:completion:arg-completer[vm] = (comp:subcommands &opts= [ help ] $vm-completions)
